# Usage

`./golangsendmail -h`

# Commandline Arguments
```
  -config string
        Pfad zur Konfigurationsdatei (default "./config.properties")
  -csv string
        Input CSV (default "./input.csv")
```

Without specifying any arguments the program will use the default values.
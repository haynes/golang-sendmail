package main

import (
	"bytes"
	"encoding/csv"
	"flag"
	"log"
	"net/smtp"
	"os"
	"text/template"

	"github.com/magiconair/properties"
)

type CsvLine struct {
	Column1 string
	Column2 string
	Column3 string
}

var (
	outfile, _ = os.Create("./golangsendmail.log")
	l          = log.New(outfile, "", 0)
)

func main() {

	// Kommandozeilen Argumente
	configPath := flag.String("config", "./config.properties", "Pfad zur Konfigurationsdatei")
	csvPath := flag.String("csv", "./input.csv", "Input CSV")
	flag.Parse()

	//Konfigurationsdatei laden
	p := properties.MustLoadFile(*configPath, properties.UTF8)

	// Properties auslesen
	host := p.MustGetString("host")
	username := p.MustGetString("username")
	password := p.MustGetString("password")
	templatePath := p.MustGetString("template")
	mailSender := p.MustGetString("sender")
	mailSubject := p.MustGetString("subject")

	lines, err := ReadCsv(*csvPath)
	if err != nil {
		panic(err)
	}

	// Loop through lines & turn into object
	for _, line := range lines {

		data := CsvLine{
			Column1: line[0],
			Column2: line[1],
			Column3: line[2],
		}

		mailText := ParseTemplate(templatePath, data)

		SendMail(host, username, password, data.Column3, mailSender, mailSubject, mailText)
	}

}

// ReadCsv accepts a file and returns its content as a multi-dimentional type
// with lines and each column. Only parses to string type.
func ReadCsv(filename string) ([][]string, error) {

	// Open CSV file
	f, err := os.Open(filename)
	if err != nil {
		return [][]string{}, err
	}
	defer f.Close()

	// Read File into a Variable
	lines, err := csv.NewReader(f).ReadAll()
	if err != nil {
		return [][]string{}, err
	}

	return lines, nil
}

// Liest ein eine Template Datei ein und befüllt die Variablen.
func ParseTemplate(filename string, variables CsvLine) string {

	var tpl bytes.Buffer

	tmpl := template.Must(template.ParseFiles(filename))

	tmpl.Execute(&tpl, variables) // merge.

	return tpl.String()
}

func SendMail(host string, username string, password string, recipient string, sender string, subject string, mailtext string) {

	// Nur Authentication versuchen wenn auch username und passwort gesetzt sind
	var auth smtp.Auth = nil
	if username != "" && password != "" {
		auth = smtp.PlainAuth("", username, password, host)
	}

	// Here we do it all: connect to our server, set up a message and send it
	to := []string{recipient}
	mime := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"
	msg := []byte("To: " + recipient + "\r\n" +
		"Subject: " + subject + "\r\n" +
		mime + "\r\n" +
		mailtext + "\r\n")

	err := smtp.SendMail(host, auth, sender, to, msg)
	if err != nil {
		l.Fatal(err)
	} else {
		l.Println("Email an folgende Mail Addresse versandt:" + recipient)
	}

}
